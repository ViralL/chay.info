import "slick-carousel";
import "jquery-easing";
import "magnific-popup";
import "select2/dist/js/select2.full";
import noUiSlider from "nouislider";
import wNumb from "wnumb";

//range slider
const stepsSlider = document.getElementById('range');
const inputLower = document.getElementById('skip-value-lower');
const inputUpper = document.getElementById('skip-value-upper');
const inputs = [inputLower, inputUpper];

if(stepsSlider) {
  noUiSlider.create(stepsSlider, {
    start: [1200, 3500],
    step: 1,
    behaviour: 'drag',
    connect: true,
    range: {
      'min': 100,
      'max': 10000
    },
    format: wNumb({
      decimals: 0
    })
  });
  stepsSlider.noUiSlider.on('update', function (values, handle) {
    inputs[handle].value = values[handle];
  });
  inputs.forEach(function (input, handle) {
    input.addEventListener('change', function () {
      stepsSlider.noUiSlider.setHandle(handle, this.value);
    });
    input.addEventListener('keydown', function (e) {
      const values = stepsSlider.noUiSlider.get();
      const value = Number(values[handle]);
      const steps = stepsSlider.noUiSlider.steps();
      const step = steps[handle];
      let position;

      switch (e.which) {
        case 13:
          stepsSlider.noUiSlider.setHandle(handle, this.value);
          break;
        case 38:
          position = step[1];
          if (position === false) {
            position = 1;
          }
          if (position !== null) {
            stepsSlider.noUiSlider.setHandle(handle, value + position);
          }
          break;
        case 40:
          position = step[0];
          if (position === false) {
            position = 1;
          }
          if (position !== null) {
            stepsSlider.noUiSlider.setHandle(handle, value - position);
          }
          break;
      }
    });
  });
}
//range slider

$(document).ready(function() {
  slidersMainCatalog();
  slidersClients();
  slidersMain();
  slidersCard();

  $(".sent--js").click(function () {
    $(this).closest('.activeForm').hide();
    $(this).closest('.activeForm').next().show();
  });

  // anchor
  $(".anchor").on("click","a", function () {
    if (this.hash !== "") {
      const hash = this.hash;
      const top = $(hash).offset().top;

      $('body,html').animate({scrollTop: top - 120}, 1600, 'easeOutQuart');
    }
    return false;
  });
  // anchor


  // mask phone {maskedinput}
  $("[name=phone]").mask("+7 (999) 999-9999");

  // popup
  $('.popup-modal').magnificPopup({
    type: 'inline',
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in',
    showCloseBtn: false,
    fixedContentPos: true,
    callbacks: {
      open: function() {
        $('.slider-for').slick('setPosition');
        $('.slider-navi').slick('setPosition');
      }
    }
  });
  $(document).on('click', '.popup-modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
  });

  // accordion
  $(".accordion").on("click", ".accordion-header", function() {
    // $(this).next().fadeOut();
    $(this)
      .toggleClass("active")
      .next()
      .slideToggle();
  });

  // tabs
  $(".tabs-list").on("click", ".tab", function() {
    const href = $(this).attr("href");
    const positionLeft = $(this).position().left;
    const width = $(this).width();
    const span = $(this).parent().find('.line');
    span.css({
      left: positionLeft,
      width: width,
    });

    $(this).closest(".tabs").find("> .tabs-list .tab").removeClass("active");
    $(this).closest(".tabs").find("> .tabs-content").removeClass("show");
    $(this).addClass("active");
    $(href).addClass("show");

    $('.designSlider--js').slick('setPosition');

    return false;
  });


  // filtering
  let x = 8;
  $(".filtering--js").on("click", ".filtering-el", function() {
    let count = $(this).data("count");
    const parent = $(this).closest('.moreDetail--js');
    const size = parent.find('.products__item').length;
    parent.find('.products__item').not(':lt('+count+')').hide();
    $(".filtering--js .filtering-el").removeClass('active');
    parent.find('.products__item:lt(' + count + ')').slideDown();
    if (count === 'all') {
      parent.find('.products__item').slideDown();
    }
    if (count >= size) {
      parent.find('.showMore-body').slideUp();
    } else {
      parent.find('.showMore-body').slideDown();
    }
    x = count;
    $(this).addClass("active");

    return false;
  });

  // showMore--js
  $(".showMore--js").click(function() {
    const parent = $(this).closest('.moreDetail--js');
    const size = parent.find('.products__item').length;
    x = (x+4 <= size) ? x+4 : size;
    parent.find('.products__item:lt('+x+')').slideDown();
    if (x >= size) {
      $(this).parent().slideUp();
      $(".filtering--js .filtering-el").removeClass('active');
      $(".filtering--js .filtering-el:last-child").addClass('active');
    }
    return false;
  });
  $(".showText--js").click(function() {
    $(this)
      .hide()
      .prev()
      .addClass('active');
  });
  $(".tooltip--js").click(function() {
    $(this).closest('.tooltipMain').find('.tooltipBody').toggleClass('active');
    return false;
  });

  // select
  function formatState (state) {
    if (!state.id) {
      return state.text;
    }
    const $state = state.element.value
      ? $(`<span>${state.text} <i class="${state.element.value.toLowerCase()}"></i></span>`)
      : state.text;
    return $state;
  };
  setTimeout(() => {
    $(".select-templating").select2({
      minimumResultsForSearch: Infinity,
      containerCssClass: 'templating',
      dropdownCssClass: 'templating',
      closeOnSelect : false,
      dropdownParent: '.orders',
    });
    $(".select-templating--nx").select2({
      minimumResultsForSearch: Infinity,
      containerCssClass: 'templating',
      dropdownCssClass: 'templating--nx',
      templateResult: formatState,
      templateSelection: formatState,
      dropdownParent: '.mainProduct .tabs',
    });
  }, 1000);

  $('.select-templating').on('select2:opening', function() {
    $('.select2-search__field').prop('readonly', true);
  });
  $('.select-templating').on("select2:select", function() {
    const block = $(this).next().find('.select2-selection__rendered');
    const elem = block.children().length;
    if(elem > 1) {
      block.addClass('active');
    }
  });
  $('.select-templating').on("select2:unselect", function() {
    const block = $(this).next().find('.select2-selection__rendered');
    const elem = block.children().length;
    if(elem < 2) {
      block.removeClass('active');
    } else {
      block.addClass('active');
    }
  });
  $(".select-templating").on("select2:open", function() {
    $(this).parent().addClass("current");
    $(".select2-results").find(".selecting").remove();
    const buts =
      "<div class='selecting'><div class='selectAll'>выбрать все</div><div class='clearAll'>очистить</div></div>";
    $(".select2-results").append(buts);
  });
  $(".select-templating").on("select2:close", function() {
    $(this).parent().removeClass("current");
  });
  $(document).on("click", ".selectAll", function() {
    const block = $(this).closest('.orders').find('.orders-el.current');
    block.find(".select-templating > option").prop("selected","selected");
    block.find(".select-templating").trigger("change");
    block.find(".select-templating").select2('close');
    block.find(".select-templating").select2('open');
    block.find(".select2-selection__rendered").addClass('active');
  });
  $(document).on("click", ".clearAll", function() {
    const block = $(this).closest('.orders').find('.orders-el.current');
    block.find('.select-templating').val(null).trigger("change");
    block.find(".select-templating").select2('close');
    block.find(".select-templating").select2('open');
    block.find(".select2-selection__rendered").removeClass('active');
  });


  // input[type=file]
  $(".input__file-js").change(function() {
    $(".input__file-js").each(function() {
      const name = this.value;
      const reWin = /.*\\(.*)/;
      let fileTitle = name.replace(reWin, "$1");
      const reUnix = /.*\/(.*)/;
      fileTitle = fileTitle.replace(reUnix, "$1");
      $(this)
        .parent()
        .parent()
        .find(".input__name-js")
        .val(fileTitle);
      $(this)
        .parent()
        .find(".btn")
        .text(`Загружено ${fileTitle}`);
    });
  });
  // input[type=file]


  // count input
  function numberWithSpaces(x) {
    const parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
  }
  function calcilateSumm(item, count) {
    const priceInput = item.closest('tr').find('.counter--js');
    const price = priceInput.data('price');
    const actualSumm = price * count;
    const setRegexSumm = numberWithSpaces(actualSumm.toFixed(2));
    priceInput.find('.value').text(`${setRegexSumm} р.`);
  }
  $('.input__number-js').change(function (event) {
    const value = event.target.value;
    calcilateSumm($(this), value);
  });
  $('.input__quantity-js .inc').click(function () {
    const $input = $(this).closest('.input__quantity-js').find('input');
    const th = $input.data('th');
    let count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(th ? `${count} ${th}` : count);
    $input.change();
    calcilateSumm($(this), count);
    return false;
  });
  $('.input__quantity-js .dec').click(function () {
    const $input = $(this).closest('.input__quantity-js').find('input');
    const th = $input.data('th');
    let count = parseInt($input.val()) + 1;
    count = count > 999 ? 1 : count;
    $input.val(th ? `${count} ${th}` : count);
    $input.change();
    calcilateSumm($(this), count);
    return false;
  });

  // input type ONLY number
  function forceNumericOnly (item) {
    return $(item).each(function()
    {
      $(item).keydown(function(e)
      {
        const key = e.charCode || e.keyCode || 0;
        // sucess backspace, tab, delete, arrows, numbers
        return (
          key == 8 ||
          key == 9 ||
          key == 46 ||
          key == 190 ||
          (key >= 37 && key <= 40) ||
          (key >= 48 && key <= 57) ||
          (key >= 96 && key <= 105));
      });
    });
  }
  forceNumericOnly(".input__number-js");

  $(".designSlider--js").slick({
    slidesToScroll: 1,
    slidesToShow: 1,
    speed: 1000,
    dots: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 1100,
        settings: {}
      }
    ]
  });

  // search
  $('.search--js').click(function() {
    $('body').addClass('fixed');
    $('.searchBar').addClass('active');
    $(this).hide();
    $('.search--close').show();
    return false;
  });
  $('.searchBar__close').click(function() {
    $('body').removeClass('fixed');
    $('.searchBar').removeClass('active');
    $('.search--js').show();
    $('.search--close').hide();
    return false;
  });
  // search


  $(".range-templating-title").click(function() {
    $(this).parent().toggleClass('active');
  });

  $(".main-nav__toggle--js").click(function() {
    $(this).toggleClass('active');
    $('.header').toggleClass('active');
    $('.main-nav__collapse').toggleClass('active');
  });
});

function closeContainer(value, e) {
  const container = $(value);
  if (container.length && !container.is(e.target) && container.has(e.target).length === 0){
    container.removeClass("active");
    $('body').removeClass('fixed');
    $('.search--js').show();
    $('.search--close').hide();
  }
}
$(document).mouseup(function (e) {
  closeContainer(".searchBar.active", e);
  closeContainer(".range-templating.active", e);
  closeContainer(".tooltipBody.active", e);
});

// const elementPosition = $('.header').offset();

$(document).scroll(function() {
  const scrollTop = $(this).scrollTop();
  if(scrollTop > 200){
    $('.header').addClass('fixed');
  } else {
    $('.header').removeClass('fixed');
  }
});

// sliders
const slidersMain = function() {
  $(".mainSlider--js").slick({
    slidesToScroll: 1,
    slidesToShow: 1,
    speed: 1000,
    dots: false,
    arrows: true,
    fade: true,
    infinite: false,
    nextArrow: $(".slick-next--ma"),
    prevArrow: $(".slick-prev--ma"),
    responsive: [
      {
        breakpoint: 1100,
        settings: {}
      }
    ]
  });
};
const slidersMainCatalog = function() {
  $(".catalogSlider--js").slick({
    slidesToScroll: 1,
    slidesToShow: 4,
    speed: 1000,
    dots: false,
    arrows: true,
    infinite: false,
    nextArrow: $(".slick-next--ct"),
    prevArrow: $(".slick-prev--ct"),
    responsive: [
      {
        breakpoint: 1250,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
        }
      }
    ]
  });
};
const slidersClients = function() {
  const $slider = $('.clientsSlider--js');
  const $progressBar = $('.progress');
  const $progressBarLabel = $( '.slider__label' );

  $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    const calc = ( (nextSlide) / (slick.slideCount-1) ) * 100;

    $progressBar
      .css('background-size', calc + '% 100%')
      .attr('aria-valuenow', calc );

    $progressBarLabel.text( calc + '% completed' );
  });

  $slider.slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    autoplay: true,
    speed: 1000,
    dots: false,
    arrows: true,
    nextArrow: $(".slick-next--cl"),
    prevArrow: $(".slick-prev--cl"),
    responsive: [
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });
};
const slidersCard = function() {
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: $(".slick-next--ca"),
    prevArrow: $(".slick-prev--ca"),
    asNavFor: '.slider-navi'
  });
  $('.slider-navi').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    arrows: false,
    focusOnSelect: true
  });
  $('.linkDots').click(function(e) {
    e.preventDefault();
    const slideno = $(this).data('slide');
    $('.slider-for').slick('slickGoTo', slideno - 1);
  });
};


// On before slide change
$('.mainSlider--js').on('init setPosition', function(){
  $(".slick-current").addClass('initSlide');
});
$('.mainSlider--js').on('beforeChange', function(){
  $(".slick-slide").removeClass('nextSlide');
  $(".slick-slide").removeClass('nextSlide');
  $(".slick-slide").removeClass('prevSlide');
  $('.slick-current').addClass('prevSlide');
  setTimeout(() => {
    $('.slick-current').addClass('nextSlide');
  }, 300);
});
$('.mainSlider--js').on('afterChange', function(){
  $(".slick-slide.prevSlide").removeClass('prevSlide');
  $(".slick-slide").removeClass('initSlide');
});

$(window).on('resize orientationchange', function(){
  $('mainSlider--js').slick('setPosition');
});
