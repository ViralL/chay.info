$(".sitebar_search").click(function() {
  $(".searchBar").addClass("active");
});
$(".searchBar__close").click(function() {
  $(".searchBar").removeClass("active");
});
$(".sitebar__sect_menu, .main-nav__toggle--js").click(function() {
  $("body").css("overflow", 'hidden');
  $(".menuBar").addClass("active");
  setTimeout(() => $(".menuBar-item").addClass("visible slideInUp"), 500);
});
$(".menuBar__close, .menuBar__closeMob").click(function() {
  $("body").css("overflow", 'visible');
  $(".menuBar").removeClass("active");
  $(".menuBar-item").removeClass("visible slideInUp");
});

$(".open--js").click(function() {
  $(this).toggleClass('active');
  $(this).next().toggleClass('active');
});
const screen_width = Math.max(
  document.documentElement.clientWidth,
  window.innerWidth || 0
);
if (screen_width <= 767) {
  $(".mainTitle--subNav h1").click(function() {
    $(this).toggleClass('active');
    $('.subNav').slideToggle();
  });
}

$(".aside__header").click(function() {
  $(this).toggleClass('active');
  $(this).next().slideToggle();
});

$(".showForm--js").click(function() {
  $(this).hide();
  $(this).parent().find('.main-content--list').hide();
  $(this).parent().find('.main-content--form').show();

  $('html, body').animate({
    scrollTop: $($(this).parent()).offset().top,
  }, 500);

  return false;
});

// Hide Header on on scroll down
$(function(){
  let lastScrollTop = 0, delta = 5;
  $(window).scroll(function(){
    const st = $(this).scrollTop();
    const setPos = st < 1300 ? st : 1300;
    $(".about__section-img").css({"right": `${setPos - 1000}px`});
    if(Math.abs(lastScrollTop - st) <= delta)
      return;

    if (st > lastScrollTop && lastScrollTop > 0){
      $("header").addClass('nav-up');
    } else {
      $("header").removeClass('nav-up');
    }
    lastScrollTop = st;
  });
});
