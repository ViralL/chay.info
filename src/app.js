/* src/app.js */

// Styles
import "styles/_app.scss";
import AOS from "aos";
import Rellax from "rellax";

require("scripts/main");
require("scripts/jquery.maskedinput.min.js");

$(document).ready(() => {
  // require("scripts/header");
  // require("scripts/pages");
});


// mobile scripts
const screen_width = Math.max(
  document.documentElement.clientWidth,
  window.innerWidth || 0
);

$(window).on("load", function() {
  window.scrollTo(0, 0);
  setTimeout(() => {
    $('.loader').addClass('hidden');

    const target = $(location).attr("hash");
    if(target) {
      $('body,html').animate({
        scrollTop: $(target).offset().top - 120
      }, 1600, 'easeOutQuart');
    }

    AOS.init();

    if (screen_width > 767) {
      const rellax = new Rellax('.rellax', {
        speed: 2,
        center: true,
        wrapper: null,
        // round: true,
        vertical: true,
        horizontal: false
      });
    }

    setTimeout(() => {
      $('.loader').css('z-index', -9999);

    }, 1000);
  }, 500);
});

